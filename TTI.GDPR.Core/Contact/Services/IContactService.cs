﻿using System.Collections.Generic;
using TTI.GDPR.Core.Contact.Models;
using TTI.GDPR.Core.Sites.Enums;

namespace TTI.GDPR.Core.Contact.Services
{
    public interface IContactService
    {
        IEnumerable<ContactResponseModel> GetContactByEmail(KenticoSite site, ContactRequestModel requestModel);

        string MakeContactAnonymous(KenticoSite site, ContactDeleteRequestModel requestModel, bool isDummyRequest);
    }
}
