﻿using System;
using System.Collections.Generic;
using System.Linq;

using CMS.Membership;
using CMS.MessageBoards;
using CMS.Newsletters;
using CMS.OnlineMarketing;
using TTI.GDPR.Core.Contact.Helpers;
using TTI.GDPR.Core.Contact.Models;
using TTI.GDPR.Core.Kentico.Constants;
using TTI.GDPR.Core.Sites.Enums;

namespace TTI.GDPR.Core.Contact.Services
{
    public class ContactService : IContactService
    {
        public IEnumerable<ContactResponseModel> GetContactByEmail(KenticoSite site, ContactRequestModel requestModel)
        {
            var contacts = ContactInfoProvider.GetContacts();

            if(requestModel.IsEmpty())
                return new List<ContactResponseModel>();

            if (!string.IsNullOrWhiteSpace(requestModel.EmailAddress))
                contacts.WhereEquals("ContactEmail", requestModel.EmailAddress);

            if (!string.IsNullOrWhiteSpace(requestModel.FirstName))
                contacts.WhereEquals("ContactFirstName", requestModel.FirstName);

            if (!string.IsNullOrWhiteSpace(requestModel.LastName))
                contacts.WhereEquals("ContactLastName", requestModel.LastName);

            var result = contacts.ToList();

            var contactResponseModels = new List<ContactResponseModel>();
            // If we don't have a result and we have an email, try and populate none-contactinfo data
            if (!result.Any() && !string.IsNullOrWhiteSpace(requestModel.EmailAddress))
            {
                contactResponseModels.Add(FillResponseModel(site, requestModel.EmailAddress));
            }
            else // else we just try fill everything, since we have some contact infos
            {
                foreach (var contact in result)
                {
                    contactResponseModels.Add(FillResponseModel(site, contact));
                }
            }
            return contactResponseModels.Where(x => x != null);
        }

        private ContactResponseModel FillResponseModel(KenticoSite site, ContactInfo contact, bool isTemporary = false)
        {
            // Set all the contact data
            var contactResponseModel = new ContactResponseModel(contact);

            // Visited Pages
            contactResponseModel.PagesVisited = GetVisitedPages(contact);
            // Newsletter Subscriptions
            contactResponseModel.NewsletterSubscriptions = GetNewsletterSubscriptions(contact);

            // Reviews
            contactResponseModel.Reviews = GetReviews(site, contact);

            // if temporary then we want to ignore the contactInfo data, as the contact doesn't exist but other data might
            if (isTemporary && contactResponseModel.IsEmpty())
            {
                return null;
            }

            return contactResponseModel;
        }

        private ContactResponseModel FillResponseModel(KenticoSite site, string emailAddress)
        {
            var contactInfo = new ContactInfo
            {
                ContactEmail = emailAddress,
            };

            // Using temporary = true as we created a temporary contact info
            return FillResponseModel(site, contactInfo, true);
        }

        private List<string> GetVisitedPages(ContactInfo contactInfo)
        {
            // return pagevisit activities
            return ActivityInfoProvider.GetActivities()
                .WhereEquals("ActivityActiveContactID", contactInfo.ContactID)
                .WhereEquals("ActivityType", ActivityNames.PageVisit)
                .Select(x => x.ActivityURL)
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .ToList();
        }

        private List<string> GetNewsletterSubscriptions(ContactInfo contactInfo)
        {
            // Get all subscribers
            var subscribers = SubscriberInfoProvider.GetSubscribers()
                .WhereEquals("SubscriberEmail", contactInfo.ContactEmail)
                .ToList();

            if (!subscribers.Any())
                return new List<string>();

            var subscribedNewsletters = new List<int>();
            foreach (var sub in subscribers)
            {
                // Lookup the newsletters for each subscriber
                var newsletters = SubscriberNewsletterInfoProvider.GetSubscriberNewsletters()
                    .WhereEquals("SubscriberID", sub.SubscriberID)
                    .Select(x => x.NewsletterID)
                    .ToList();

                foreach (var n in newsletters)
                {
                    if (!subscribedNewsletters.Contains(n))
                    {
                        // Add it if we don't have it, just incase duplicate subscribers with the same email have additional subscriptions
                        subscribedNewsletters.Add(n);
                    }
                }
            }

            // Do a lookup and return
            return NewsletterInfoProvider.GetNewsletters()
                .WhereIn("NewsletterID", subscribedNewsletters)
                .Select(x => x.NewsletterDisplayName)
                .ToList();
        }

        public List<Review> GetReviews(KenticoSite site, ContactInfo contactInfo)
        {
            var reviews = new List<Review>();

            // Do site specific things
            switch (site)
            {
                case KenticoSite.Milwaukee:
                    reviews = BoardMessageInfoProvider.GetMessages()
                        .WhereEquals("MessageEmail", contactInfo.ContactEmail)
                        .Select(x => new Review(x))
                        .ToList();
                    break;

                case KenticoSite.Ryobi:
                    reviews = BoardMessageInfoProvider.GetMessages()
                        .WhereEquals("MessageEmail", contactInfo.ContactEmail)
                        .Select(x => new Review(x))
                        .ToList();
                    break;
            }

            return reviews;
        }

        public string MakeContactAnonymous(KenticoSite site, ContactDeleteRequestModel requestModel, bool isDummyRequest)
        {
            var temporaryContact = false;
            if (isDummyRequest)
            {
                return "Dummy request from the Swagger UI - No changes have been made";
            }

            // Get the contacts
            var contacts = ContactInfoProvider.GetContacts()
            .WhereEquals("ContactEmail", requestModel.EmailAddress)
            .ToList();

            // Create an empty contact info just to ensure none-contact data can be deleted later on
            if (!contacts.Any() && !string.IsNullOrWhiteSpace(requestModel.EmailAddress))
            {
                // We are using a temporary contact
                temporaryContact = true;
                contacts.Add(new ContactInfo
                {
                    ContactEmail = requestModel.EmailAddress,
                });
            }

            // Generate one email for all of the existing contacts of the searched email address
            var newEmail = CustomContactHelper.GenerateAnonymousEmail();
            foreach (var contact in contacts)
            {
                var oldEmail = contact.ContactEmail;

                contact.ContactEmail = newEmail;
                contact.ContactFirstName = "";
                contact.ContactMiddleName = "";
                // Required - Sets it to the same format as Kentico
                contact.ContactLastName = $"Anonymous - {DateTime.Now:yyyy-MM-dd HH:mm:ss.fff}";
                contact.ContactSalutation = "";
                contact.ContactBirthday = DateTime.MinValue;
                contact.ContactGender = (int) UserGenderEnum.Unknown;
                contact.ContactAddress1 = "";
                contact.ContactAddress2 = "";
                contact.ContactCity = "";
                contact.ContactZIP = "";
                contact.ContactMobilePhone = "";
                contact.ContactBusinessPhone = "";
                contact.ContactHomePhone = "";
                contact.ContactCompanyName = "";
                contact.ContactJobTitle = "";
                contact.ContactMergedWithContactID = 0;

                // Newsletter Subscriptions
                AnonymiseNewsletterSubscriptions(oldEmail, newEmail);

                // Reviews
                AnonymiseReviews(site, oldEmail, newEmail);

                if (temporaryContact == false)
                {
                    ContactInfoProvider.SetContactInfo(contact, true);
                }
            }

            if (contacts.Any())
            {
                return newEmail;
            }
            return "";
        }

        public void AnonymiseNewsletterSubscriptions(string oldEmailAddress, string newEmailAddress)
        {
            var subscribers = SubscriberInfoProvider.GetSubscribers()
                .WhereEquals("SubscriberEmail", oldEmailAddress)
                .ToList();

            foreach (var sub in subscribers)
            {
                sub.SubscriberEmail = newEmailAddress;
                sub.SubscriberFirstName = "";
                sub.SubscriberLastName = "";
                sub.SubscriberFullName = "";
                SubscriberInfoProvider.SetSubscriberInfo(sub);
            }
        }

        private void AnonymiseReviews(KenticoSite site, string oldEmail, string newEmail)
        {
            var reviews = BoardMessageInfoProvider.GetMessages()
                .WhereEquals("MessageEmail", oldEmail);

            foreach (var review in reviews)
            {
                review.MessageEmail = newEmail;
                BoardMessageInfoProvider.SetBoardMessageInfo(review);
            }
        }
    }
}
