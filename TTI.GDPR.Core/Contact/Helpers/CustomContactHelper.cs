﻿using System;
using System.Security.Cryptography;
using System.Text;
using CMS.Membership;

namespace TTI.GDPR.Core.Contact.Helpers
{
    public static class CustomContactHelper
    {
        public static string GetGenderFromInt(int gender)
        {
            switch ((UserGenderEnum)gender)
            {
                case UserGenderEnum.Female:
                    return "Female";
                case UserGenderEnum.Male:
                    return "Male";
                case UserGenderEnum.Unknown:
                    return "Unknown";
                default:
                    return "Unknown";
            }
        }

        public static string GenerateAnonymousEmail()
        {
            using (var md5 = MD5.Create())
            {
                // Generate a new guid
                var prefixBytes = Encoding.UTF8.GetBytes(Guid.NewGuid().ToString());
                var postfix = "anonymous";

                // Get a hash from the guid
                var hash = md5.ComputeHash(prefixBytes);

                // Convert the hexadecimal values
                var sb = new StringBuilder();
                foreach (var t in hash)
                {
                    sb.Append(t.ToString("X2"));
                }

                var newEmailPrefix = sb.ToString();

                return $"{newEmailPrefix}@{postfix}.co.uk";
            }
        }
    }
}
