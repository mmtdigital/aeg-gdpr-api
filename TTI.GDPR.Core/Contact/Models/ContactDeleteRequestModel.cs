﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTI.GDPR.Core.Contact.Models
{
    public class ContactDeleteRequestModel
    {
        public string EmailAddress { get; set; }
    }
}
