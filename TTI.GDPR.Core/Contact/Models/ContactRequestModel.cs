﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTI.GDPR.Core.Contact.Models
{
    public class ContactRequestModel
    {
        /// <summary>
        ///     Email Address
        /// </summary>
        public string EmailAddress { get; set; }
        /// <summary>
        ///     First Name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        ///     Last Name
        /// </summary>
        public string LastName { get; set; }

        public bool IsEmpty()
        {
            return string.IsNullOrWhiteSpace(EmailAddress) && string.IsNullOrWhiteSpace(FirstName) && string.IsNullOrWhiteSpace(LastName);
        }
    }
}
