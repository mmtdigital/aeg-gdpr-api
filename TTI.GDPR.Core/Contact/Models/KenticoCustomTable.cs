﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTI.GDPR.Core.Contact.Models
{
    public class KenticoCustomTable
    {
        public string CustomTableName { get; set; }
        public DataTable CustomTableData { get; set; }

        public KenticoCustomTable()
        {

        }

        public KenticoCustomTable(string customTableName, DataTable customTableData)
        {
            CustomTableName = customTableName;
            CustomTableData = customTableData;
        }
    }
}
