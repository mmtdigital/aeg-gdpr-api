﻿using CMS.Globalization;
using CMS.OnlineMarketing;

namespace TTI.GDPR.Core.Contact.Models
{
    public class Address
    {
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }

        public Address()
        {
            
        }

        public Address(ContactInfo contactInfo)
        {
            Line1 = contactInfo.ContactAddress1;
            Line2 = contactInfo.ContactAddress2;
            City = contactInfo.ContactCity;
            ZipCode = contactInfo.ContactZIP;
            Country = CountryInfoProvider.GetCountryInfo(contactInfo.ContactCountryID)?.CountryDisplayName ?? "";
        }
    }
}
