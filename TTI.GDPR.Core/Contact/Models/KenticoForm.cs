﻿using System.Data;

namespace TTI.GDPR.Core.Contact.Models
{
    public class KenticoForm
    {
        public string FormName { get; set; }
        public DataTable FormData { get; set; }

        public KenticoForm()
        {
            
        }

        public KenticoForm(string formName, DataTable formData)
        {
            FormName = formName;
            FormData = formData;
        }
    }
}
