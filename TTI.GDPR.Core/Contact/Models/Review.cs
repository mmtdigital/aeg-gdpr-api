﻿using System;
using CMS.MessageBoards;

namespace TTI.GDPR.Core.Contact.Models
{
    public class Review
    {
        public string ReviewName { get; set; }
        public string ReviewText { get; set; }
        public DateTime Created { get; set; }
        public double RatingValue { get; set; }

        public Review(BoardMessageInfo messageInfo)
        {
            ReviewName = messageInfo.MessageUserName;
            ReviewText = messageInfo.MessageText;
            RatingValue = messageInfo.MessageRatingValue;
            Created = messageInfo.MessageInserted;
        }
    }
}
