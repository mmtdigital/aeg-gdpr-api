﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using CMS.OnlineMarketing;
using TTI.GDPR.Core.Contact.Helpers;
using TTI.GDPR.Core.Sites.Enums;

namespace TTI.GDPR.Core.Contact.Models
{
    public class ContactResponseModel
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Salutation { get; set; }
        public string Title { get; set; }
        public DateTime Birthday { get; set; }
        public string Gender { get; set; }
        public string CompanyName { get; set; }
        public string JobTitle { get; set; }
        public Address Address { get; set; }
        public string MobilePhone { get; set; }
        public string HomePhone { get; set; }
        public string BusinessPhone { get; set; }
        public string EmailAddress { get; set; }


        public List<string> Tags { get; set; }
        public List<string> PagesVisited { get; set; }
        public List<KenticoForm> FormEntries { get; set; }
        public List<KenticoCustomTable> CustomTables { get; set; }
        public List<string> NewsletterSubscriptions { get; set; }
        public DataTable ProductsOwned { get; set; }
        public List<Review> Reviews { get; set; }


        public ContactResponseModel()
        {
            
        }

        public ContactResponseModel(ContactInfo contactInfo)
        {
            FirstName = contactInfo.ContactFirstName;
            MiddleName = contactInfo.ContactMiddleName;
            LastName = contactInfo.ContactLastName;
            Salutation = contactInfo.ContactSalutation;
            Title = contactInfo.ContactTitleBefore;
            Birthday = contactInfo.ContactBirthday;
            Gender = CustomContactHelper.GetGenderFromInt(contactInfo.ContactGender);
            CompanyName = contactInfo.ContactCompanyName;
            JobTitle = contactInfo.ContactJobTitle;
            Address = new Address(contactInfo);
            MobilePhone = contactInfo.ContactMobilePhone;
            HomePhone = contactInfo.ContactHomePhone;
            BusinessPhone = contactInfo.ContactBusinessPhone;
            EmailAddress = contactInfo.ContactEmail;
            Tags = contactInfo
                .GetStringValue("ContactTags", "")
                .Split(';')
                .ToList()
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .ToList();
        }

        public bool IsEmpty()
        {
            if (!Tags.Any() &&
                !PagesVisited.Any() &&
                !FormEntries.Any() &&
                !CustomTables.Any() &&
                !NewsletterSubscriptions.Any() &&
                (ProductsOwned == null || ProductsOwned.Rows.Count == 0) &&
                !Reviews.Any())
            {
                return true;
            }

            return false;
        }
    }
}
