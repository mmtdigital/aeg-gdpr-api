﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TTI.GDPR.Core.Sites.Enums
{
    public enum KenticoSite
    {
        Milwaukee = 1,
        Ryobi = 2,
        AEG = 3
    }
}
