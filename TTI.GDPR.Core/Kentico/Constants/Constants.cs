﻿namespace TTI.GDPR.Core.Kentico.Constants
{
    public static class ActivityNames
    {
        public const string PageVisit = "pagevisit";
    }

    public static class Roles
    {
        public const string AuthorisedApiRole = "TTI.GDPR.Web.Api.User";
    }
}
