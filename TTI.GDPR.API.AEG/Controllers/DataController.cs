﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Swashbuckle.Examples;
using Swashbuckle.Swagger.Annotations;
using TTI.GDPR.API.Core.Controllers;
using TTI.GDPR.API.Core.Swagger.Examples;
using TTI.GDPR.Core.Contact.Models;
using TTI.GDPR.Core.Contact.Services;
using TTI.GDPR.Core.Sites.Enums;

namespace TTI.GDPR.API.AEG.Controllers
{
    public class DataController : AbstractDataController
    {
        public DataController(IContactService contactService) : base(contactService)
        {
        }

        /// <summary>
        ///     Queries Kentico Contacts based on the given information.
        /// </summary>
        /// <param name="requestModel">Request Model</param>
        /// <returns></returns>
        [SwaggerResponse(HttpStatusCode.OK, "Get Contacts", typeof(IEnumerable<ContactResponseModel>))]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "Unauthorised Login Details")]
        [SwaggerResponse(HttpStatusCode.NotFound, "Contact not found")]
        [SwaggerRequestExample(typeof(ContactRequestModel), typeof(ContactRequestModelExample))]
        public override HttpResponseMessage Get([FromBody]ContactRequestModel requestModel)
        {
            List<ContactResponseModel> contactDetails = null;

                contactDetails = ContactService.GetContactByEmail(KenticoSite.AEG, requestModel).ToList();

                if (!contactDetails.Any())
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }

            return Request.CreateResponse(HttpStatusCode.OK, contactDetails);
        }

        /// <summary>
        ///     Anonymises the Contact and all other Data using the given Email Address (This will only anonymise data when called directly from an API call)
        /// </summary>
        /// <param name="requestModel">Request Model</param>
        /// <returns></returns>
        [SwaggerResponse(HttpStatusCode.OK, "Delete Contacts")]
        [SwaggerResponse(HttpStatusCode.Unauthorized, "Unauthorised Login Details")]
        [SwaggerResponse(HttpStatusCode.NotFound, "Contact not found")]
        [SwaggerRequestExample(typeof(ContactDeleteRequestModel), typeof(ContactDeleteRequestModelExample))]
        public override HttpResponseMessage Delete([FromBody]ContactDeleteRequestModel requestModel)
        {
            var responseMessage = ContactService.MakeContactAnonymous(KenticoSite.AEG, requestModel, IsDummyRequest);

            if (string.IsNullOrWhiteSpace(responseMessage))
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, responseMessage);
        }
    }
}