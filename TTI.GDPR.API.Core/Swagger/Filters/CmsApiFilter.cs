﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Description;
using Swashbuckle.Swagger;

namespace TTI.GDPR.API.Core.Swagger.Filters
{
    public class CmsApiFilter : IDocumentFilter
    {
        public void Apply(SwaggerDocument swaggerDoc, SchemaRegistry schemaRegistry, IApiExplorer apiExplorer)
        {
            // Removing /cmsapi for post requests
            swaggerDoc.paths
                .Where(x => x.Key.StartsWith("/cmsapi"))
                .ToList()
                .ForEach(x => x.Value.post = null);

            // Removing /cmsapi for get requests
            swaggerDoc.paths
                .Where(x => x.Key.StartsWith("/cmsapi"))
                .ToList()
                .ForEach(x => x.Value.get = null);

            // Removing /cmsapi for delete requests
            swaggerDoc.paths
                .Where(x => x.Key.StartsWith("/cmsapi"))
                .ToList()
                .ForEach(x => x.Value.delete = null);
        }
    }
}
