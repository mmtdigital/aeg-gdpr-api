﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Filters;
using Swashbuckle.Swagger;
using TTI.GDPR.API.Core.Swagger.Constants;

namespace TTI.GDPR.API.Core.Swagger.Filters
{
    class AuthenticationFilter : IOperationFilter
    {
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            var filterPipeline = apiDescription.ActionDescriptor.GetFilterPipeline();
            var isAuthorized = filterPipeline
                .Select(filterInfo => filterInfo.Instance)
                .Any(filter => filter is IAuthorizationFilter);

            var allowAnonymous = apiDescription.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();

            if (isAuthorized && !allowAnonymous)
            {
                operation.parameters.Add(new Parameter
                {
                    name = "Username",
                    @in = "header",
                    description = "Username",
                    required = true,
                    type = "string"
                });

                operation.parameters.Add(new Parameter
                {
                    name = "Password",
                    @in = "header",
                    description = "Password",
                    required = true,
                    type = "string"
                });
            }
        }
    }
}
