﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Swashbuckle.Examples;
using TTI.GDPR.Core.Contact.Models;

namespace TTI.GDPR.API.Core.Swagger.Examples
{
    public class ContactDeleteRequestModelExample : IExamplesProvider
    {
        public object GetExamples()
        {
            return new ContactDeleteRequestModel
            {
                EmailAddress = "test@test.co.uk",
            };
        }
    }
}
