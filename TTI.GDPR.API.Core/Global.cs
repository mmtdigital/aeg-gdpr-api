﻿using System;
using System.Web;
using System.Web.Http;
using CMS.DataEngine;

namespace TTI.GDPR.API.Core
{
    public class Global : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            // Initialise the Kenico providers if not already.
            CMSApplication.Init();
        }
    }
}