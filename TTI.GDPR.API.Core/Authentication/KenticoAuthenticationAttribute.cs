﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Filters;
using CMS.Membership;
using CMS.OnlineMarketing;
using CMS.SiteProvider;
using TTI.GDPR.API.Core.Authentication.Models;
using TTI.GDPR.API.Core.Swagger.Constants;
using TTI.GDPR.Core.Kentico.Constants;

namespace TTI.GDPR.API.Core.Authentication
{
    public class KenticoAuthenticationFilter : IAuthenticationFilter
    {
        public bool AllowMultiple { get; }
        public async Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            // 1. Look for credentials in the request.
            HttpRequestMessage request = context.Request;
            AuthenticationHeaderValue authorization = request.Headers.Authorization;

            // Try get out the Swagger Login from the headers
            if (authorization == null)
            {
                if (request.Headers.Contains("Username") && request.Headers.Contains("Password"))
                {
                    authorization = HandleSwaggerAuthentication(ref request);
                }
                else
                {
                    context.ErrorResult = new AuthenticationFailureResult("Invalid credentials", request);
                    return;
                }
            }

            // 3. If there are credentials but the filter does not recognize the 
            //    authentication scheme, do nothing.
            if (authorization.Scheme != "Basic")
            {
                return;
            }

            // 4. If there are credentials that the filter understands, try to validate them.
            // 5. If the credentials are bad, set the error result.
            if (string.IsNullOrEmpty(authorization.Parameter))
            {
                context.ErrorResult = new AuthenticationFailureResult("Missing credentials", request);
                return;
            }

            var authenticated = AuthenticateWithKentico(authorization.Parameter);
            if (authenticated)
            {
                var indentity = new GenericIdentity(authorization.Parameter);
                context.Principal = new GenericPrincipal(indentity, null);
            }
            else
            {
                context.ErrorResult = new AuthenticationFailureResult("Invalid credentials", request);
            }
        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            return Task.FromResult(0);
        }

        private static bool AuthenticateWithKentico(string authKey)
        {
            var bytes = Convert.FromBase64String(authKey);
            var result = Encoding.ASCII.GetString(bytes);

            // Splits on the first occurence into 2 strings
            var split = result.Split(new[] { ':' }, 2);

            if (!split.Any())
                return false;

            if (split.Length != 2)
                return false;

            // Extract username and Password
            var userName = split[0];
            var password = split[1];

            // Authenticate with Kentico
            var apiUser = AuthenticationHelper.AuthenticateUser(userName, password, SiteContext.CurrentSiteName);
            if (apiUser != null)
            {
                // Check Role
                if (CheckRole(apiUser))
                {
                    return true;
                }
            }

            return false;
        }

        private static AuthenticationHeaderValue HandleSwaggerAuthentication(ref HttpRequestMessage request)
        {
            var username = request.Headers.GetValues("Username").FirstOrDefault();
            var password = request.Headers.GetValues("Password").FirstOrDefault();
            var bytes = Encoding.UTF8.GetBytes($"{username}:{password}");
            var authKey = Convert.ToBase64String(bytes);

            // Add a custom header when we are coming from the swagger API
            request.Headers.Add(SwaggerHeaders.DummyApiCall, "true");

            return new AuthenticationHeaderValue("Basic", authKey);
        }

        private static bool CheckRole(UserInfo user)
        {
            var roles = RoleInfoProvider.GetAllRoles(SiteContext.CurrentSiteID);
            var apiRole = roles.FirstOrDefault(x => x.RoleName == Roles.AuthorisedApiRole);

            if (apiRole == null)
                return false;

            var userApiRole = UserRoleInfoProvider.GetUserRoles()
                .WhereEquals("UserID", user.UserID)
                .WhereEquals("RoleID", apiRole.RoleID).FirstObject;

            return userApiRole == null ? false : true;
        }
    }
}