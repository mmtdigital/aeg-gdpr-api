﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Swashbuckle.Examples;
using Swashbuckle.Swagger.Annotations;
using TTI.GDPR.API.Core.Swagger.Constants;
using TTI.GDPR.API.Core.Swagger.Examples;
using TTI.GDPR.Core.Contact.Models;
using TTI.GDPR.Core.Contact.Services;

namespace TTI.GDPR.API.Core.Controllers
{
    public abstract class AbstractDataController : ApiController
    {
        public IContactService ContactService;
        public bool IsDummyRequest => Request.Headers.Contains(SwaggerHeaders.DummyApiCall);

        protected AbstractDataController(IContactService contactService)
        {
            ContactService = contactService;
        }

        [Authorize]
        [HttpPost]
        public abstract HttpResponseMessage Get(ContactRequestModel requestModel);

        [Authorize]
        [HttpDelete]
        public abstract HttpResponseMessage Delete(ContactDeleteRequestModel requestModel);
    }
}
