﻿using System;
using System.Linq;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using Newtonsoft.Json.Serialization;
using TTI.GDPR.API.Core.Authentication;
using TTI.GDPR.Core.Contact.Services;

namespace TTI.GDPR.API.Core
{
    public class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Add camel case
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;

            // Web attribute routing.
            config.MapHttpAttributeRoutes();

            // Authentication
            config.Filters.Add(new KenticoAuthenticationFilter());

            // Convention-based routing.
            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}/{action}/{id}",
                new { id = RouteParameter.Optional }
            );


            SetupAutofac();
        }

        private static void SetupAutofac()
        {
            // Autofac Setup
            var builder = new ContainerBuilder();

            // Get your HttpConfiguration.
            var config = GlobalConfiguration.Configuration;

            // Register your Web API controllers.
            var assemblies = AppDomain.CurrentDomain.GetAssemblies()
                .Where(x => x.FullName.StartsWith("TTI."))
                .ToArray();

            builder.RegisterApiControllers(assemblies);

            // Register Services
            builder.RegisterType<ContactService>().AsImplementedInterfaces().InstancePerLifetimeScope();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}